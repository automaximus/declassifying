import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe
import skimage
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage import data
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
import operator
from skimage.color import rgb2gray
import time
import os

tf.reset_default_graph()
tfe.enable_eager_execution(device_policy=tfe.DEVICE_PLACEMENT_SILENT)

image = skimage.io.imread('jpg/batch2-19.jpg')
image = rgb2gray(image)

LARGE_SIZE = 120
SMALL_SIZE = 30
squares = np.zeros(shape=(image.shape[0] // SMALL_SIZE, image.shape[1] // SMALL_SIZE, LARGE_SIZE, LARGE_SIZE))
for i in range(0, image.shape[0] - LARGE_SIZE, SMALL_SIZE):
    for j in range(0, image.shape[1] - LARGE_SIZE, SMALL_SIZE):
        squares[i // SMALL_SIZE, j // SMALL_SIZE] = image[i:i + LARGE_SIZE, j:j + LARGE_SIZE]


def unpool(value, name='unpool'):
    """N-dimensional version of the unpooling operation from
    https://www.robots.ox.ac.uk/~vgg/rg/papers/Dosovitskiy_Learning_to_Generate_2015_CVPR_paper.pdf

    :param value: A Tensor of shape [b, d0, d1, ..., dn, ch]
    :return: A Tensor of shape [b, 2*d0, 2*d1, ..., 2*dn, ch]
    """
    with tf.name_scope(name) as scope:
        sh = value.get_shape().as_list()
        dim = len(sh[1:-1])
        out = (tf.reshape(value, [-1] + sh[-dim:]))
        for i in range(dim, 0, -1):
            out = tf.concat(axis=i, values=[out, tf.zeros_like(out)])
        out_size = [-1] + [s * 2 for s in sh[1:-1]] + [sh[-1]]
        out = tf.reshape(out, out_size, name=scope)
    return out


class SchemeAutoencoder(tfe.Network):
    def __init__(self, squares, batch_size, large_size, small_size):
        super(SchemeAutoencoder, self).__init__()
        self.squares = squares
        self.large_size = large_size
        self.small_size = small_size

        self._input_shape = [batch_size, large_size, large_size, 1]

        self.cv1 = self.track_layer(tf.layers.Conv2D(32, 5, padding='SAME'))
        self.mp1 = self.track_layer(tf.layers.MaxPooling2D(
            (2, 2), (2, 2), padding='same'))
        self.cv2 = self.track_layer(tf.layers.Conv2D(16, 5, padding='SAME'))
        self.mp2 = self.track_layer(tf.layers.MaxPooling2D(
            (2, 2), (2, 2), padding='same'))
        self.cv3 = self.track_layer(tf.layers.Conv2D(8, 5, padding='SAME'))

        self.dense = self.track_layer(tf.layers.Dense(512))

        self.cv4 = self.track_layer(tf.layers.Conv2DTranspose(16, 5, padding='SAME'))
        self.cv5 = self.track_layer(tf.layers.Conv2DTranspose(32, 5, padding='SAME'))
        self.cv6 = self.track_layer(tf.layers.Conv2DTranspose(1, 5, padding='SAME'))

    def encoder(self, inputs):
        x1 = tf.reshape(inputs, self._input_shape)
        x1 = self.cv3(self.mp2(self.cv2(self.mp1(self.cv1(x1)))))
        self.dense(x1)
        return x1

    def decoder(self, inputs):
        x2 = self.cv6(unpool(self.cv5(unpool(self.cv4(inputs)))))
        return x2

    def call(self, inputs):
        x1 = tf.reshape(inputs, self._input_shape)
        x1 = self.cv3(self.mp2(self.cv2(self.mp1(self.cv1(x1)))))
        x1 = self.dense(x1)
        x2 = self.cv4(x1)
        x3 = unpool(x2)
        x4 = self.cv5(x3)
        x5 = unpool(x4)
        x6 = self.cv6(x5)
        return x6

    def prepare_training_example(self, pos_x1, pos_y1):
        image1 = self.squares[pos_x1, pos_y1]
        return image1.astype(np.float32)

    def generator(self):
        while True:
            i1 = np.random.randint(squares.shape[0])
            j1 = np.random.randint(squares.shape[1])
            x = self.prepare_training_example(i1, j1)
            yield x


def loss(model, input):
    print(model(input))
    print(input)
    print(model(input) - input)
    diff = tf.square(model(input) - input)
    print(diff)
    l = tf.reduce_mean(diff)
    print(l)
    return l


checkpoint_directory = 'autoencoder_log/model_002_asymmetric/'
epoch_size = 100
log_interval = 100

LEARN = True

model = SchemeAutoencoder(squares, 1, LARGE_SIZE, SMALL_SIZE)
optimizer = tf.train.AdamOptimizer(learning_rate=0.001)

summary_writer = tf.contrib.summary.create_summary_file_writer(
    checkpoint_directory, flush_millis=1000)
checkpoint_prefix = os.path.join(checkpoint_directory, 'ckpt')
latest_cpkt = tf.train.latest_checkpoint(checkpoint_directory)
if latest_cpkt:
    print('Using latest checkpoint at ' + latest_cpkt)

device = "gpu:0" if tfe.num_gpus() else "cpu:0"
print("Using device: %s" % device)

with tf.device(device):
    if LEARN:
        for epoch in range(1, 101):
            with tfe.restore_variables_on_create(latest_cpkt):
                global_step = tf.train.get_or_create_global_step()
                start = time.time()
                total_loss = 0
                with summary_writer.as_default():
                    with tf.contrib.summary.record_summaries_every_n_global_steps(log_interval):
                        for step in range(epoch_size):
                            x = tf.constant(next(model.generator()), dtype=tf.float32)
                            optimizer.minimize(lambda: loss(model, x), global_step=global_step)

                            # Tensorboard summary
                            l = loss(model, x)
                            total_loss += l
                            if step % log_interval == 0:
                                tf.contrib.summary.scalar('Image_norm', tf.norm(x))
                                tf.contrib.summary.scalar('Loss', l)
                                x = tf.constant(next(model.generator()), dtype=tf.float32)
                                tf.contrib.summary.image(
                                    'Generated_images', model.call(x), max_images=500)

                        tf.contrib.summary.scalar('Total_loss', total_loss)

                end = time.time()
                print('Train time for epoch #%d (global step %d):   %f' %
                      (epoch, global_step.numpy(), end - start))
                print('Loss for epoch #%d (global step %d):     %f' %
                      (epoch, global_step.numpy(), total_loss))

                all_variables = (
                        model.variables + optimizer.variables() + [global_step])
                tfe.Saver(all_variables).save(checkpoint_prefix, global_step=global_step)

    if not LEARN:
        with tfe.restore_variables_on_create(latest_cpkt):
            # Initializing variables
            x = tf.constant(next(model.generator()), dtype=tf.float32)
            model(x)

            model_distances = {}

            for i in range(squares.shape[0]):
                for j in range(squares.shape[1]):
                    model_distances[(i, j)] = tf.norm(model.encoder(squares[i, j], x)).numpy()

            sorted_distances = sorted(model_distances.items(), key=operator.itemgetter(1))[:20]
            for pair in sorted_distances:
                index = pair[0]
                d = pair[1]
                plt.imshow(np.concatenate([squares[index[0], index[1]], x]))
                plt.title(str(d.numpy()))
                plt.show()


