import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe
import skimage
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage import data
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
from skimage.transform import rescale
import operator
from skimage.color import rgb2gray
import time
import os

tf.reset_default_graph()
tfe.enable_eager_execution(device_policy=tfe.DEVICE_PLACEMENT_SILENT)

image = skimage.io.imread(r'C:\Users\Daniels\PycharmProjects\deClassify\jpg\batch2-2.jpg')
image = rgb2gray(image)
print(image.shape)

LARGE_SIZE = 120
SMALL_SIZE = 20


# squares = np.zeros(shape=(image.shape[0] // SMALL_SIZE, image.shape[1] // SMALL_SIZE, LARGE_SIZE, LARGE_SIZE),
#                   dtype=np.float32)


class SchemeAutoencoder(tfe.Network):
    def __init__(self, image, batch_size, large_size, small_size):
        super(SchemeAutoencoder, self).__init__()
        self.image = image
        self.large_size = large_size
        self.small_size = small_size
        self.batch_size = batch_size

        self._input_shape = [batch_size, large_size, large_size,
                             1]
        self._single_input_shape = [1, large_size, large_size,
                                    1]

        self.cv1 = self.track_layer(tf.layers.Conv2D(32, 5, padding='SAME'))
        self.cv2 = self.track_layer(tf.layers.Conv2D(16, 5, padding='SAME'))
        self.cv3 = self.track_layer(tf.layers.Conv2D(8, 5, padding='SAME'))

        self.dense1 = self.track_layer(tf.layers.Dense(512))
        self.dense2 = self.track_layer(tf.layers.Dense(512))
        self.dense3 = self.track_layer(tf.layers.Dense(512))

        self.cv4 = self.track_layer(tf.layers.Conv2DTranspose(16, 5, padding='SAME'))
        self.cv5 = self.track_layer(tf.layers.Conv2DTranspose(32, 5, padding='SAME'))
        self.cv6 = self.track_layer(tf.layers.Conv2DTranspose(1, 5, padding='SAME'))

        self.loss = []

    def encoder(self, inputs):
        x1 = tf.reshape(inputs, self._single_input_shape)
        x1 = self.cv3(self.cv2(self.cv1(x1)))
        x1 = self.dense1(x1)
        x1 = self.dense2(x1)
        return x1

    def decoder(self, inputs):
        x2 = self.dense3(inputs)
        x2 = self.cv6(self.cv5(self.cv4(x2)))
        return x2

    def call(self, inputs):
        x1 = tf.reshape(inputs, self._input_shape)
        x1 = self.cv3(self.cv2(self.cv1(x1)))
        x1 = self.dense1(x1)
        x1 = self.dense2(x1)

        x2 = self.dense3(x1)
        x2 = self.cv6(self.cv5(self.cv4(x2)))
        return x2

    def prepare_training_example(self, pos_x1, pos_y1):
        image1 = self.image[pos_x1 * self.small_size:pos_x1 * self.small_size + self.large_size,
                 pos_y1 * self.small_size:pos_y1 * self.small_size + self.large_size]
        return image1.astype(np.float32)

    def generator(self):
        while True:
            i1 = np.random.randint(image.shape[0] // self.small_size)
            j1 = np.random.randint(image.shape[1] // self.small_size)
            x = self.prepare_training_example(i1, j1)
            while not is_square_regular(x):
                i1 = np.random.randint(image.shape[0] // self.small_size)
                j1 = np.random.randint(image.shape[1] // self.small_size)
                x = self.prepare_training_example(i1, j1)
            yield x

    def batch_generator(self):
        batch = []
        for _ in range(self.batch_size):
            batch.append(next(self.generator()))
        return batch


def loss(model, input):
    input = tf.reshape(input, model._input_shape)
    l = tf.reduce_mean(tf.square(model(input) - input))
    model.loss.append(l)
    return l



def is_square_regular(input):
    centre = input[input.shape[0] // 3:input.shape[0] // 3 + input.shape[0] // 3,
             input.shape[1] // 3:input.shape[1] // 3 + input.shape[1] // 3]
    min_norm = 0.0001
    max_norm = np.linalg.norm(np.ones(shape=centre.shape))
    if np.linalg.norm(centre) < min_norm or np.linalg.norm(centre) > max_norm - min_norm:
        return False
    if input.shape != (120, 120):
        return False
    return True


checkpoint_directory = 'C:/Users/Daniels/PycharmProjects/deClassify/autoencoder_log/model_last/'
epoch_size = 200
log_interval = 100

LEARN = True

model = SchemeAutoencoder(image, 1, LARGE_SIZE, SMALL_SIZE)
optimizer = tf.train.RMSPropOptimizer(learning_rate=0.001)

summary_writer = tf.contrib.summary.create_summary_file_writer(
    checkpoint_directory, flush_millis=1000)
checkpoint_prefix = os.path.join(checkpoint_directory, 'ckpt')
latest_cpkt = tf.train.latest_checkpoint(checkpoint_directory)
if latest_cpkt:
    print('Using latest checkpoint at ' + latest_cpkt)

device = "gpu:0" if tfe.num_gpus() else "cpu:0"
print("Using device: %s" % device)

with tf.device(device):
    if LEARN:
        for epoch in range(1, 201):
            with tfe.restore_variables_on_create(latest_cpkt):
                global_step = tf.train.get_or_create_global_step()
                start = time.time()
                with summary_writer.as_default():
                    with tf.contrib.summary.record_summaries_every_n_global_steps(log_interval):
                        for step in range(epoch_size):
                            batch = tf.constant(model.batch_generator(), dtype=tf.float32)
                            optimizer.minimize(lambda: loss(model, batch), global_step=global_step)

                    tf.contrib.summary.scalar('Total_loss', np.average(model.loss[-epoch_size:-1]))

                end = time.time()
                print('Train time for epoch #%d (global step %d):   %f' %
                      (epoch, global_step.numpy(), end - start))
                print('Loss for epoch #%d (global step %d):     %f' %
                      (epoch, global_step.numpy(), np.average(model.loss[-epoch_size:-1])))

                all_variables = (
                        model.variables + optimizer.variables() + [global_step])
                tfe.Saver(all_variables).save(checkpoint_prefix, global_step=global_step)

    if not LEARN:
        with tfe.restore_variables_on_create(latest_cpkt):

            batch = tf.constant(model.batch_generator(), dtype=tf.float32)
            model(batch)

            x = tf.constant(next(model.generator()), dtype=tf.float32)
            '''
            vectors = np.zeros(dtype=np.float32, shape=[squares.shape[0], squares.shape[1], 40, 40, 128])
            for i in range(squares.shape[0]):
                for j in range(squares.shape[1]):
                    if not (np.linalg.norm(squares[i, j]) < 0.0001 or np.linalg.norm(squares[i, j]) > np.linalg.norm(
                            np.ones(shape=x.shape)) - 0.0001):
                        vectors[i, j] = model.encoder(squares[i, j]).numpy()
            '''

            while True:
                # Initializing variables
                x = tf.constant(next(model.generator()), dtype=tf.float32)

                x = image[680:680 + LARGE_SIZE, 360:360 + LARGE_SIZE]
                plt.imshow(x)
                plt.show()

                if np.linalg.norm(x) < 0.0001 or np.linalg.norm(x) > np.linalg.norm(np.ones(shape=x.shape)) - 0.0001:
                    print('Black or white')
                    continue

                model_distances = {}

                x_encoded = model.encoder(x)
                for i in range(image.shape[0] // SMALL_SIZE):
                    for j in range(image.shape[1] // SMALL_SIZE):
                        if not (np.linalg.norm(image[i: i * SMALL_SIZE + LARGE_SIZE,
                                               j:j * SMALL_SIZE + LARGE_SIZE]) < 0.0001 or np.linalg.norm(
                            image[i: i * SMALL_SIZE + LARGE_SIZE, j:j * SMALL_SIZE + LARGE_SIZE]) > np.linalg.norm(
                            np.ones(shape=x.shape)) - 0.0001):
                            model_distances[(i, j)] = tf.reduce_mean(
                                tf.square(x_encoded - model.encoder(
                                    image[i: i * SMALL_SIZE + LARGE_SIZE, j:j * SMALL_SIZE + LARGE_SIZE]))).numpy()
                        else:
                            model_distances[(i, j)] = 9999999.0

                sorted_distances = sorted(model_distances.items(), key=operator.itemgetter(1))[:20]
                for pair in sorted_distances:
                    index = pair[0]
                    d = pair[1]
                    plt.imshow(np.concatenate([image[index[0]:index[0] * SMALL_SIZE + LARGE_SIZE,
                                               index[1]:index[1] * SMALL_SIZE + LARGE_SIZE], x]))
                    plt.title(str(d))
                    plt.show()

## model_001: without dense layer.
## model_002: dense, 512

# Izbaci bele kvadrate. I crne?
