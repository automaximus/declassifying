import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe
import skimage
from skimage import data
import matplotlib.pyplot as plt
from skimage.transform import rescale
import operator
from skimage.color import rgb2gray
import time
import pandas as pd
import os

tf.reset_default_graph()
tfe.enable_eager_execution(device_policy=tfe.DEVICE_PLACEMENT_SILENT)

all_labels = pd.read_csv('data/output/all_labels.csv')
all_labels['WIDTH'] = all_labels['XMAX']-all_labels['XMIN']
all_labels['HEIGHT'] = all_labels['YMAX']-all_labels['YMIN']
images = {}
for filename in all_labels['IMAGE'].drop_duplicates().values:
    images[filename] = rgb2gray(skimage.io.imread('data/jpg/' + filename))

classes = all_labels['LABEL'].drop_duplicates().reset_index(drop=True)
num_classes = all_labels['LABEL'].drop_duplicates().shape[0]
shuffled = all_labels.sample(frac=1).reset_index(drop=True)
train_set = shuffled.loc[:1900]
test_set = shuffled.loc[1900:]

print("There are {} classes.".format(num_classes))
print("Lenght of the train set is {}, and length of the test set is {}".format(train_set.shape[0], test_set.shape[0]))


class PureConv(tfe.Network):
    def __init__(self, num_classes, train_set, test_set, classes, images):
        super(PureConv, self).__init__()

        self.num_classes = num_classes
        self.train_set = train_set
        self.test_set = test_set
        self.classes = classes
        self.images = images

        self._input_shape = [1, None, None, 1]

        self.cv1 = self.track_layer(tf.layers.Conv2D(32, 5, padding='SAME'))
        self.mp1 = self.track_layer(tf.layers.MaxPooling2D((2, 2), (2, 2), padding='same'))
        self.cv2 = self.track_layer(tf.layers.Conv2D(16, 5, padding='SAME'))
        self.mp2 = self.track_layer(tf.layers.MaxPooling2D((2, 2), (2, 2), padding='same'))
        self.cv3 = self.track_layer(tf.layers.Conv2D(8, 5, padding='SAME'))

        self.logits = self.track_layer(tf.layers.Conv2D(self.num_classes, [1, 1], padding='SAME'))

        self.loss = []

    def call(self, inputs):
        x = tf.reshape(inputs, [1, inputs.shape[0], inputs.shape[1], 1])
        x = self.cv1(x)
        x = self.mp1(x)
        x = self.cv2(x)
        x = self.mp2(x)
        x = self.cv3(x)
        logits = self.logits(x)
        return logits

    def prepare_training_example(self, pos):
        row = self.train_set.loc[pos]
        image = self.images[row['IMAGE']]
        data = (image[row['XMIN']:row['XMAX'], row['YMIN']:row['YMAX']]).astype(np.float32)
        label = self.classes[self.classes == row['LABEL']].index[0]
        return data, label

    def train_generator(self):
        while True:
            i = np.random.randint(self.train_set.shape[0])
            data, label = self.prepare_training_example(i)
            yield data, label


def loss(model, x, y):
    l = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=model(x), labels=[y]))
    model.loss.append(l)
    return l


checkpoint_directory = 'pure_conv_log/model_001/'
epoch_size = 500
log_interval = 100

model = PureConv(num_classes, train_set, test_set, classes, images)
optimizer = tf.train.RMSPropOptimizer(learning_rate=0.001)

summary_writer = tf.contrib.summary.create_summary_file_writer(
    checkpoint_directory, flush_millis=1000)
checkpoint_prefix = os.path.join(checkpoint_directory, 'ckpt')
latest_cpkt = tf.train.latest_checkpoint(checkpoint_directory)
if latest_cpkt:
    print('Using latest checkpoint at ' + latest_cpkt)

device = "gpu:0" if tfe.num_gpus() else "cpu:0"
print("Using device: %s" % device)

with tf.device(device):
    for epoch in range(1, 201):
        with tfe.restore_variables_on_create(latest_cpkt):
            global_step = tf.train.get_or_create_global_step()
            start = time.time()
            with summary_writer.as_default():
                with tf.contrib.summary.record_summaries_every_n_global_steps(log_interval):
                    for step in range(epoch_size):
                        train_example, label = next(model.train_generator())
                        x = tf.constant(train_example, dtype=tf.float32)
                        y = tf.constant(label, dtype=tf.float32)
                        print(x.shape)
                        optimizer.minimize(lambda: loss(model, x, y),
                                           global_step=global_step)

                        if step % log_interval == 0 and step > 0:
                            tf.contrib.summary.scalar('Loss', np.average(model.loss[-log_interval:-1]))
            end = time.time()

            tf.contrib.summary.scalar('Epoch Loss', np.average(model.loss[-epoch_size:-1]))
            print('Train time for epoch #%d (global step %d):   %f' %
                  (epoch, global_step.numpy(), end - start))
            print('Loss for epoch #%d (global step %d):     %f' %
                  (epoch, global_step.numpy(), np.average(model.loss[-epoch_size:-1])))

            all_variables = (
                    model.variables + optimizer.variables() + [global_step])
            tfe.Saver(all_variables).save(checkpoint_prefix, global_step=global_step)
