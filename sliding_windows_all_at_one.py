import numpy as np
import matplotlib.pyplot as plt
import cv2


x_coord_shape = 3508
y_coord_shape = 4962


# 0 - check valve
# 1 - system logic instrumented protection
# 2 - triangle with A inside
# 3 - valves
# 4 - detecting code numbers


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    mask = image.copy()
    mask[:] = 255

    # perform the actual rotation and return the image
    return (cv2.warpAffine(image, M, (nW, nH), borderValue=0),
            cv2.warpAffine(mask, M, (nW, nH), borderValue=0))


def find_symbol(image, shape_path, angles, coeff):
    templates = []
    for angle in angles:
        tpl = cv2.imread(shape_path, 0)
        tpl = cv2.resize(tpl, (0, 0), fx=image.shape[0] / x_coord_shape, fy=image.shape[1] / y_coord_shape)
        tpl, mask = rotate_bound(tpl, angle)
        templates.append({'image': tpl, 'mask': mask})

    ### Do template matching for each rotation of template image
    match_results = [cv2.matchTemplate(image, tpl['image'], cv2.TM_SQDIFF, mask=tpl['mask'])
                     for tpl in templates]

    ### Filter results by a*stdev from mean

    filtered = [np.uint8(match_result < np.mean(match_result.ravel()) - coeff * np.std(match_result.ravel()))
                for match_result in match_results]

    return filtered, templates


def draw_symbols(filtered, templates, col, matched, num=False):
    for i in range(len(templates)):
        tpl = templates[i]['image']
        tpl_h, tpl_w = tpl.shape

        ret, labels = cv2.connectedComponents(np.uint8(filtered[i]))

        corner_points = []

        for label in np.unique(labels[labels != 0]):
            where = np.where(labels == label)
            if num:
                corner_points.append((where[1][0] - 10, where[0][0]))
            else:
                corner_points.append((where[1][0], where[0][0]))

        for corner in corner_points:
            if num:
                matched = cv2.rectangle(matched,
                                        corner, tuple(np.add(corner, (tpl_w + 220, tpl_h))),
                                        color=col, thickness=3)
            else:
                matched = cv2.rectangle(matched,
                                        corner, tuple(np.add(corner, (tpl_w, tpl_h))),
                                        color=col, thickness=3)
        return matched


def do_everything(image_path, num=False):
    img = cv2.imread(image_path, 0)

    if num:
        # code numbers
        filtered, templates = find_symbol(image=img,
                                            shape_path=r"jpg\primeri\primer_navodnici.png",
                                            angles=[0, -45, -90, -135, -180, -225, -270, -315], coeff=3.15)
        matched = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

        matched = draw_symbols(filtered=filtered, templates=templates, col=(255, 0, 0), matched=matched, num=True)

        fig, ax = plt.subplots(figsize=(35, 25))
    else:
        # check valve
        filtered0, templates0 = find_symbol(image=img, shape_path=r"jpg\primeri\napred.png",
                                            angles=[0, -45, -90, -135, -180, -225, -270, -315], coeff=11)
        # system logic instrumented protection
        filtered1, templates1 = find_symbol(image=img,
                                            shape_path=r"jpg\primeri\neki_romb.png",
                                            angles=[0, -45, -90], coeff=5.5)
        # triangle with A inside
        filtered2, templates2 = find_symbol(image=img, shape_path=r"jpg\primeri\trougao.png",
                                            angles=[0], coeff=6)
        # valves
        filtered3, templates3 = find_symbol(image=img, shape_path=r"jpg\primeri\tpl.png",
                                            angles=[0, -45, -90, 45, 90], coeff=8.5)

        matched = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

        matched1 = draw_symbols(filtered=filtered0, templates=templates0, col=(138, 43, 226), matched=matched)
        matched2 = draw_symbols(filtered=filtered1, templates=templates1, col=(0, 255, 0), matched=matched)
        matched3 = draw_symbols(filtered=filtered2, templates=templates2, col=(0, 0, 255), matched=matched)
        matched4 = draw_symbols(filtered=filtered3, templates=templates3, col=(255, 0, 0), matched=matched)

        fig, ax = plt.subplots(figsize=(35, 25))

    return ax, matched, matched1, matched2, matched3, matched4


def classify(num=False):
    for i in range(1, 5):
        ax, graphhics, graphics1, graphics2, graphics3, graphics4 = do_everything(
            r'jpg\primeri\6-6-201718-05-06_Part' + str(i) + '_0.png', num=num)
        ax.imshow(graphhics)
        ax.imshow(graphics1)
        ax.imshow(graphics2)
        ax.imshow(graphics3)
        ax.imshow(graphics4)

    plt.show()

# if none argument - doing symbol classification
# in argument "True" doing code number detection
classify()
