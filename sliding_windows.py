# %matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

import cv2


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    mask = image.copy()
    mask[:] = 255

    # perform the actual rotation and return the image
    return (cv2.warpAffine(image, M, (nW, nH), borderValue=0),
            cv2.warpAffine(mask, M, (nW, nH), borderValue=0))


img = cv2.imread(r"C:\Users\Daniels\Downloads\jpg\primeri\6-6-201718-05-06_Part1_0.png", 0)

# fig, ax = plt.subplots()
filtered = []

pic1 = 0
# 0 - check valve
# 1 - system logic instrumented protection
# 2 - triangle with A inside
# 3 - detecting numbers

# check valve
if pic1 == 0:
    angles = [0, -45, -90, 45, 90]
    templates = []
    for angle in angles:
        tpl = cv2.imread(r"C:\Users\Daniels\Downloads\jpg\primeri\tpl.png", 0)
        tpl = cv2.resize(tpl, (0, 0), fx=img.shape[0] / 3508, fy=img.shape[1] / 4961)
        tpl, mask = rotate_bound(tpl, angle)
        templates.append({'image': tpl, 'mask': mask})

    ### Do template matching for each rotation of template image
    match_results = [cv2.matchTemplate(img, tpl['image'], cv2.TM_SQDIFF, mask=tpl['mask'])
                     for tpl in templates]

    ### Filter results by a*stdev from mean

    filtered = [np.uint8(match_result < np.mean(match_result.ravel()) - 8.5 * np.std(match_result.ravel()))
                for match_result in match_results]


# system logic instrumented protection
elif pic1 == 1:
    angles = [0, -45, -90, 45, 90]
    templates = []
    for angle in angles:
        tpl = cv2.imread(r"C:\Users\Daniels\Downloads\jpg\primeri\neki_romb.png", 0)
        tpl = cv2.resize(tpl, (0, 0), fx=img.shape[0] / 3508, fy=img.shape[1] / 4961)
        tpl, mask = rotate_bound(tpl, angle)
        templates.append({'image': tpl, 'mask': mask})

    ### Do template matching for each rotation of template image
    match_results = [cv2.matchTemplate(img, tpl['image'], cv2.TM_SQDIFF, mask=tpl['mask'])
                     for tpl in templates]

    ### Filter results by a*stdev from mean

    filtered = [np.uint8(match_result < np.mean(match_result.ravel()) - 5.5 * np.std(match_result.ravel()))
                for match_result in match_results]

elif pic1 == 2:
    angles = [0, -45, -90, 45, 90]
    templates = []
    for angle in angles:
        tpl = cv2.imread(r"C:\Users\Daniels\Downloads\jpg\primeri\trougao.png", 0)
        tpl = cv2.resize(tpl, (0, 0), fx=img.shape[0] / 3508, fy=img.shape[1] / 4961)
        tpl, mask = rotate_bound(tpl, angle)
        templates.append({'image': tpl, 'mask': mask})

    ### Do template matching for each rotation of template image
    match_results = [cv2.matchTemplate(img, tpl['image'], cv2.TM_SQDIFF, mask=tpl['mask'])
                     for tpl in templates]

    ### Filter results by a*stdev from mean

    filtered = [np.uint8(match_result < np.mean(match_result.ravel()) - 6 * np.std(match_result.ravel()))
                for match_result in match_results]

elif pic1 == 3:
    angles = [0, -45, -90, 45, 90]
    templates = []
    for angle in angles:
        tpl = cv2.imread(r"C:\Users\Daniels\Downloads\jpg\primeri\primer_navodnici.png", 0)
        tpl = cv2.resize(tpl, (0, 0), fx=img.shape[0] / 3508, fy=img.shape[1] / 4961)
        tpl, mask = rotate_bound(tpl, angle)
        templates.append({'image': tpl, 'mask': mask})

    ### Do template matching for each rotation of template image
    match_results = [cv2.matchTemplate(img, tpl['image'], cv2.TM_SQDIFF, mask=tpl['mask'])
                     for tpl in templates]

    ### Filter results by a*stdev from mean

    filtered = [np.uint8(match_result < np.mean(match_result.ravel()) - 3.29 * np.std(match_result.ravel()))
                for match_result in match_results]

### Drawing on the original image

matched = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

for i in range(len(templates)):
    tpl = templates[i]['image']
    tpl_h, tpl_w = tpl.shape

    ret, labels = cv2.connectedComponents(np.uint8(filtered[i]))
    corner_points = []
    if pic1 == 3:
        for label in np.unique(labels[labels != 0]):
            where = np.where(labels == label)
            corner_points.append((where[1][0] - 10, where[0][0]))

        for corner in corner_points:
            matched = cv2.rectangle(matched,
                                    corner, tuple(np.add(corner, (tpl_w + 220, tpl_h))),
                                    color=(255, 0, 0), thickness=3)
    else:
        for label in np.unique(labels[labels != 0]):
            where = np.where(labels == label)
            corner_points.append((where[1][0], where[0][0]))

        for corner in corner_points:
            matched = cv2.rectangle(matched,
                                    corner, tuple(np.add(corner, (tpl_w, tpl_h))),
                                    color=(255, 0, 0), thickness=3)

fig, ax = plt.subplots(figsize=(35, 25))
ax.imshow(matched)
plt.show()
